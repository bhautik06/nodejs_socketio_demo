const { on } = require("process");

const app = require("express")();
const http = require("http").Server(app);
const io = require("socket.io")(http);
const port = 3000;

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/rooms.html");
});

app.get("/createUser", function (req, res) {
  res.sendFile(__dirname + "/users.html");
});

app.get("/joinroom", function (req, res) {
  res.sendFile(__dirname + "/joinroom.html");
});

app.get("/displayroom", function (req, res) {
  res.sendFile(__dirname + "/displayRoom.html");
});

let users = [];

let rooms = [];

io.on("connection", (socket) => {
  console.log("user connected");
  socket.on("createRoom", (roomname) => {
    if (roomname) {
      let roomData = {
        id: socket.id,
        roomname: roomname,
        users: [],
      };
      rooms.push(roomData);
      //   console.log(roomData);
    }
  });

  socket.on("createUser", (username) => {
    if (username) {
      let userDetail = {
        id: socket.id,
        username: username,
        rooms: [],
      };
      users.push(userDetail);
      //   console.log(userDetail);
    }
  });

  socket.emit("getroom", rooms);

  socket.on("joinRoom", ({ username, roomname }) => {
    const findUser = users.find((user) => user.username === username);
    const findRoom = rooms.find((room) => room.roomname === roomname);
    if (findUser && findRoom) {
      for (const user of users) {
        if (user.username === username) {
          user.rooms.push(findRoom.roomname);
        }
      }
      for (const room of rooms) {
        if (room.roomname === roomname && room.users.length < 5) {
          socket.join(room.id);
          room.users.push(findUser.username);
        }
      }
    }
  });

  socket.on("displayroom", (roomname) => {
    let roomDetail;
    let allrooms = rooms.find((room) => room.roomname === roomname);
    roomDetail = allrooms;
    socket.emit("rooms", roomDetail);
  });

  socket.on("disconnect", () => {
    console.log("user disconnected");
  });
});

http.listen(port, function () {
  console.log(`server running on port ${port}`);
});
